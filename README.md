# Differences for the standard profile

rwprofile.info.yml
--
- comment
- contact

rwprofile.profile
--
- use Drupal\contact\Entity\ContactForm;
- use Drupal\Core\Form\FormStateInterface;

delete hook: Implements hook_form_FORM_ID_alter() for install_configure_form().
delete hook: Submission handler to sync the contact.form.feedback recipient.

config
--
config/install/system.theme.yml
-default: olivero
+default: holy

config/install/user.settings.yml
-register: visitors_admin_approval
+register: admin_only

config/install/user.role.authenticated.yml
- contact
- 'access site-wide contact form'

config/install/user.role.anonymous.yml
- contact
- 'access site-wide contact form'

delete file: config/install/contact.form.feedback.yml

config/install/core.entity_form_display.node.article.default.yml
- field.field.node.article.comment
- comment
- comment:
    type: comment_default
    weight: 20
    region: content
    settings: {  }
    third_party_settings: {  }

config/install/core.entity_view_display.node.article.default.yml
- core.entity_view_display.comment.comment.default
- field.field.node.article.comment
- comment
- comment:
    type: comment_default
    label: above
    settings:
      view_mode: default
      pager_id: 0
    third_party_settings: {  }
    weight: 110
    region: content

config/install/core.entity_view_display.node.article.teaser.yml
- field.field.node.article.comment
- hidden:
    comment: true

core.entity_view_display.node.article.rss.yml
- field.field.node.article.comment
- comment: true

delete file: config/install/comment.type.comment.yml
delete file: config/install/field.field.node.article.comment.yml
delete file: config/install/field.storage.node.comment.yml
delete file: config/install/core.entity_form_display.comment.comment.default.yml
delete file: config/install/core.entity_view_display.comment.comment.default.yml
delete file: config/install/field.field.comment.comment.comment_body.yml


Add field to article
--
field_gallery
field_date
