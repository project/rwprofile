(function ($, Drupal) {
  var slider = tns({
    container: '.view-intro .view-content',
    items: 1,
    slideBy: 'page',
    autoplay: true,
    controls: true,
    nav: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    autoplayButtonOutput: false,
    controlsText: ['<', '>'],
    mouseDrag: true,
    responsive:{
      0:{
        controls:false,
        autoplay:false,
      },
      1400:{
        controls:true,
        autoplay:1,
      }
    }
  });
})(window.jQuery, window.Drupal);
